/* In this task, you will implement a class which represents
 * arbitrary-size natural numbers (including 0). In addition to the
 * methods prescribed below, the class must support the following:
 *
 *  • arithmetic operators ‹+›, ‹-›, ‹*›, ‹/› and ‹%› (the last two
 *    implementing division and reminder),
 *  • all relational operators,
 *  • bitwise operators ‹^› (xor), ‹&› (and) and ‹|› (or).
 *
 * The usual preconditions apply (divisors are not 0, the second
 * operand of subtraction is not greater than the first).
 */

// This code deserves straight up C, there is a lot of repetitive code
// but after spending ~30 hours in 2 days on it i can't be bothered
// I will clean it up afterwards for the B

#include <functional>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <utility>
#include <vector>

class natural {
    /*
     *friend void test_print(natural, std::string msg);
     */
    // numbers stored in Least significant byte first
    std::vector<uint8_t> bytes;

    natural bitwise(const natural &right,
                    std::function<uint8_t(uint8_t, uint8_t)> op) const {
        natural result(0);
        auto this_it = bytes.begin();
        auto right_it = right.bytes.begin();

        while (this_it != bytes.end() || right_it != right.bytes.end()) {
            uint8_t this_byte = 0;
            uint8_t right_byte = 0;

            if (this_it != bytes.end()) {
                this_byte = *this_it;
                ++this_it;
            }

            if (right_it != right.bytes.end()) {
                right_byte = *right_it;
                ++right_it;
            }
            result.bytes.push_back(op(this_byte, right_byte));
        }
        return result;
    }

  public:
    /* Construct a natural number, optionally from an integer. Throw
     * ‹std::out_of_range› if ‹v› is negative. */
    explicit natural(int v = 0) {
        if (v < 0)
            throw std::out_of_range("Naturals cant be negative");
        if (v == 0)
            return;

        while (v != 0) {
            bytes.push_back(v & (0b11111111));
            v = v >> 8;
        }
    }

    bool operator<(const natural &right) const {
        auto this_it = bytes.begin();
        auto right_it = right.bytes.begin();
        bool smaller = false;
        while (this_it != bytes.end() || right_it != right.bytes.end()) {
            uint8_t this_byte = 0;
            uint8_t right_byte = 0;

            if (this_it != bytes.end()) {
                this_byte = *this_it;
                ++this_it;
            }

            if (right_it != right.bytes.end()) {
                right_byte = *right_it;
                ++right_it;
            }
            if (!(this_byte == right_byte)) {
                smaller = this_byte < right_byte;
            }
        }
        return smaller;
    }

    bool operator>(const natural &right) const {
        auto this_it = bytes.begin();
        auto right_it = right.bytes.begin();
        bool bigger = false;
        while (this_it != bytes.end() || right_it != right.bytes.end()) {
            uint8_t this_byte = 0;
            uint8_t right_byte = 0;

            if (this_it != bytes.end()) {
                this_byte = *this_it;
                ++this_it;
            }

            if (right_it != right.bytes.end()) {
                right_byte = *right_it;
                ++right_it;
            }
            if (!(this_byte == right_byte)) {
                bigger = this_byte > right_byte;
            }
        }
        return bigger;
    }

    bool operator>=(const natural &right) const { return !(*this < right); }

    bool operator<=(const natural &right) const { return !(*this > right); }

    bool operator==(const natural &right) const {
        auto this_it = bytes.begin();
        auto right_it = right.bytes.begin();

        while (this_it != bytes.end() || right_it != right.bytes.end()) {
            uint8_t this_byte = 0;
            uint8_t right_byte = 0;

            if (this_it != bytes.end()) {
                this_byte = *this_it;
                ++this_it;
            }

            if (right_it != right.bytes.end()) {
                right_byte = *right_it;
                ++right_it;
            }
            if (this_byte != right_byte)
                return false;
        }
        return true;
    }

    bool operator!=(const natural &right) const { return !(*this == right); }

    natural operator+(const natural &right) const {
        natural result(0);
        auto this_it = bytes.begin();
        auto right_it = right.bytes.begin();

        uint16_t byte_result = 0;
        while (this_it != bytes.end() || right_it != right.bytes.end()) {
            uint8_t this_byte = 0;
            uint8_t right_byte = 0;

            if (this_it != bytes.end()) {
                this_byte = *this_it;
                ++this_it;
            }

            if (right_it != right.bytes.end()) {
                right_byte = *right_it;
                ++right_it;
            }
            // overflow
            byte_result = this_byte + right_byte + (byte_result >> 8);
            result.bytes.push_back(byte_result);
        }
        if ((byte_result >> 8) == 1)
            result.bytes.push_back(1);
        return result;
    }

    natural operator-(const natural &right) const {
        natural result(0);
        auto this_it = bytes.begin();
        auto right_it = right.bytes.begin();

        uint8_t byte_result = 0;
        uint8_t overflow = 0;
        while (this_it != bytes.end() || right_it != right.bytes.end()) {
            uint8_t this_byte = 0;
            uint8_t right_byte = 0;

            if (this_it != bytes.end()) {
                this_byte = *this_it;
                ++this_it;
            }

            if (right_it != right.bytes.end()) {
                right_byte = *right_it;
                ++right_it;
            }
            // overflow
            byte_result = this_byte - right_byte - overflow;
            result.bytes.push_back(byte_result);
            if (byte_result + right_byte > this_byte)
                overflow = 1;
            else
                overflow = 0;
        }
        if (overflow == 1)
            result.bytes.push_back(1);
        return result;
    }

    natural operator*(const natural &right) const {
        // long division treating bytes as digits, (base 255)
        natural result(0);
        for (std::size_t i = 0; i < right.bytes.size(); ++i) {
            natural semi_result(0);
            for (std::size_t j = 0; j < i; ++j) {
                semi_result.bytes.push_back(0);
            }
            uint16_t r = 0;
            for (auto byte : bytes) {
                r = (byte * right.bytes[i]) + (r >> 8);
                semi_result.bytes.push_back((r << 8) >> 8);
            }
            if ((r >> 8) > 0)
                semi_result.bytes.push_back(r >> 8);
            result = result + semi_result;
        }
        return result;
    }

    std::pair<natural, natural> div_mod(const natural &right) const {
        natural zero(0);
        natural one(1);
        natural result(0);
        if (*this == zero)
            return std::make_pair(zero, zero);
        if (*this < right)
            return std::make_pair(zero, *this);
        if (right == one)
            return std::make_pair(*this, zero);
        if (*this == right)
            return std::make_pair(one, zero);
        
        //creates bunch of zeroes at the start
        natural divident(0);
        auto this_it = bytes.rbegin();
        while (this_it != bytes.rend()) {
            divident.bytes.insert(divident.bytes.begin(), *this_it);
            ++this_it;

            // subtract divisor from the partial divident
            uint8_t times = 0;
            while (divident >= right) {
                divident = divident - right;
                ++times;
            }
            result.bytes.insert(result.bytes.begin(), times);
            // divident keeps the remainder and we repeat
        }
        return std::make_pair(result, divident);
    }
    natural operator/(const natural &right) const {
        return div_mod(right).first;
    }

    natural operator%(const natural &right) const {
        return div_mod(right).second;
    }
    natural operator^(const natural &right) const {
        return bitwise(right, [](auto l, auto r) { return l ^ r; });
    }

    natural operator&(const natural &right) const {
        return bitwise(right, [](auto l, auto r) { return l & r; });
    }

    natural operator|(const natural &right) const {
        return bitwise(right, [](auto l, auto r) { return l | r; });
    }

    natural power(natural exponent) const {
        natural result = *this;
        natural one(1);
        natural zero(0);
        if (exponent == zero)
            return one;
        while (exponent > one) {
            result = result * (*this);
            exponent = exponent - one;
        }
        return result;
    }
    natural digit_count(natural base) const {
        natural copy = *this;
        natural result(0);
        natural one(1);
        natural zero(0);
        while (copy != zero) {
            copy = copy / base;
            result = result + one;
        }
        return result;
    };

    natural digit_sum(natural base) const {
        natural copy = *this;
        natural result(0);
        natural zero(0);
        while (copy != zero) {
            result = result + (copy % base);
            copy = copy / base;
        }
        return result;
    };
};

/*
 *void test_print(natural);
 */
