/* Implement methods and functions from ‹natural.hpp› here. */
#include "natural.hpp"
#include <cassert>
#include <climits>

/*
 *std::string test_binary_string(uint8_t n) {
 *    std::string str;
 *    for (int i = 0; i < 8; i++) {
 *        str.append(std::to_string(n & 1));
 *        n = n >> 1;
 *    }
 *    return std::string(str.rbegin(), str.rend());
 *}
 *
 *void test_print(natural n, std::string msg = "") {
 *    std::cout << msg << "Reversed binary representantion: ";
 *    for (auto byte : n.bytes) {
 *        std::string str = test_binary_string(byte);
 *        std::cout << std::string(str.rbegin(), str.rend()) << " ";
 *    }
 *    std::cout << std::endl;
 *};
 *natural prime(int i){
 *            natural x(i);
 *            return x;
 *        }
 */
int main() {
    { // test test lol
        //assert("00000110" == //test_binary_string(0b110));
    }
    { // basic test
        natural n960(960);
        natural n321(321);
        natural n1281(1281);
        // //test_print(n960, "n960 ");
        // //test_print(n1281, "n1281 ");
        // //test_print(n960 + n321, "n1281 should be this.");
        assert(n960 + n321 == n1281);
    }

    // zero test
    natural n0;
    // //test_print(n0+n0, "1 ");
    assert((n0 + n0) == n0);

    // subtraction test
    natural n1(1);
    // //test_print(n1-n1, "2 ");
    assert(n1 - n1 == n0);
    natural n255(255);
    natural n256(256);
    assert(n256 - n255 == n1);

    natural b00000100_00000001(0b0000010000000001);
    natural b00000001_11111111(0b0000000111111111);
    natural n514(514);

    // bitwise test
    natural n67(67);
    natural n255and67(255 & 67);
    natural n255or67(255 | 67);
    natural n255xor67(255 ^ 67);
    natural n256xor67(256 ^ 67);
    assert((n255 & n67) == n255and67);
    assert((n255 | n67) == n255or67);
    assert((n255 ^ n67) == n255xor67);
    assert((n256 ^ n67) == n256xor67);
    // multiply test
    /*
     *test_print(n255*n0, "3 ");
     */
    assert(n255 * n0 == n0);
    natural n2(2);
    natural n4(4);
    //test_print(n2 * n2, "n2*n2 ");
    assert((n2 * n2) == n4);

    assert(b00000100_00000001 - b00000001_11111111 == n514);

    // relational test
    natural nMAX(INT_MAX);
    assert(nMAX > n0);
    assert(nMAX >= n0);
    //test_print(nMAX - n1, "m ");
    //test_print(nMAX, "mm ");
    assert((nMAX - n1) < nMAX);
    assert(nMAX > (nMAX - n1));
    assert(n4 > n2);
    assert(n2 < n4);

    // INT_MAX test
    //test_print(n255 + n255, "n255*2: ");
    //test_print(nMAX, "nmax: ");
    //test_print(nMAX + nMAX, "real nmax*2: ");
    //test_print((nMAX * n2), "nmax*2: ");
    //test_print((nMAX * n2) - nMAX, "nmax*2-nmax: ");
    assert((nMAX * n2) - nMAX == nMAX);
    assert((nMAX + n1) - n1 == nMAX);
    // //test_print(nMAX - nMAX, "4 ");
    natural nMAX255 = nMAX * n255;
    // //std::cout << size_t(INT_MAX) * 255 * 255 * 255 * 255 * long(INT_MAX)
    //           << std::endl;
    //test_print(nMAX * n255 * n255 * n255 * n255 * nMAX, "loonk ");
    //test_print(nMAX + n1, ".... ");
    assert(nMAX * n255 - nMAX * n255 == n0);
    assert(nMAX - nMAX == n0);
    assert(nMAX * n256 - nMAX * n255 == nMAX);
    natural nMAX_min1(INT_MAX - 1);
    assert(nMAX - n1 == nMAX_min1);

    // division test
    natural n255div67(255 / 67);
    natural nMAXdivMAXhalf(INT_MAX / (INT_MAX / 2));
    natural nMAXhalf(INT_MAX / 2);
    assert((n255 / n67) == n255div67);
    assert(nMAXdivMAXhalf == (nMAX / nMAXhalf));
    natural nMAXdiv2(INT_MAX / 2);
    //test_print(nMAX / n2, "plsss: ");
    //test_print(nMAXdiv2, "this should be it: ");
    assert(nMAX / n2 == nMAXdiv2);
    assert(n255 / n255 == n1);
    assert(n255 / n1 == n255);
    assert(n67 / n255 == n0);
    assert(n0 / n514 == n0);
    // power test
    natural n10(10);
    assert(n2.power(n2) == n4);
    assert(nMAX.power(n10) == (nMAX * nMAX * nMAX * nMAX * nMAX * nMAX * nMAX *
                                  nMAX * nMAX * nMAX));
    //time test
    natural nMAXpow255 = nMAX.power(n255);

    natural n6(6);
    natural n420(420);
    natural n69(69);
    //test_print(n420/n69," 6: ");
    //std::cout << (255 / 20) << " " << (100 / 255) << std::endl;
    assert(n420/n69 == n6);
    assert(n420%n69 == n6);
    natural n512(512);
    assert(n512/n255 == n2);
    assert(n512%n255 == n2);
    natural n7(7);
    natural n29(29);
    assert(n512/n69 == n7);
    assert(n512%n69 == n29);
    
    // digit_.. test
    natural n3(3);
    natural n16(16);
    natural n15(15);
    natural n42(42);
    //std::cout << "before\n";
    assert(n420 / n10 == n42);
    //std::cout <<"?\n";
    assert(n420.digit_count(n10) == n3);
    assert(n420.digit_sum(n10) == n6);
    assert(n420.digit_count(n16) == n3); 
    assert(n420.digit_sum(n16) == n15);
    //std::cout << "FINISHED!!!!!" << std::endl;

    { // verity test 1
        natural one( 1 );
        natural two( 2 );
        natural three( 3 );
        natural sixteen( 16 );
        natural base( 5 );        // b = 5 
        natural power( 1 );       // p = 1 
        base = base.power( power );
        natural j( 26 );          // j_ = 26 
        natural x = base.power( j ) + one;
        assert(x.digit_sum( base ) == two);
        /*
         *test_print(x, "X: ");
         *test_print(x.digit_sum(base), "digit sum: ");
         *test_print(base, "base: ");
         *auto hmm = x.div_mod(base);
         *test_print(hmm.first, "hmm first: ");
         *test_print(hmm.second, "hmm second: ");
         *auto hmm2 = hmm.first.div_mod(base);
         *test_print(hmm2.first, "hmm2 first: ");
         *test_print(hmm2.second, "hmm2 second: ");
         *auto hmm3 = hmm2.first.div_mod(base);
         *test_print(hmm3.first, "hmm3 first: ");
         *test_print(hmm3.second, "hmm3 second: ");
         */
    }

    { // verity test 2
        natural prime(3);
        natural n( 2 << 29 );
        natural m( 10007 * 10007 );
        m = m * m * m * m;
        natural gcd_( 1 );
        natural lcm_( m * n );
        natural m_0 = m;
        natural n_0 = n;
        n = n * prime;       // prime( i ) = 3 
        n_0 = n_0 * prime;   // prime( i ) = 3 
        lcm_ = lcm_ * prime; // prime( i ) = 3 
        assert(n * m / m == n);
    }
        // verity test 3 , i assume failed because of division too
/*
 *    {
 *        
 *        natural one( 1 );
 *        natural n( 1 );
 *        natural m( 1 );
 *        natural b( 172169 );        // base = 172169
 *        natural e( 0 );           // i = 0
 *        natural p = prime( 2 );   // prime( i ) = 2
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e = prime( 1 );           // i = 1
 *        p = prime( 3 );   // prime( i ) = 3
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e= prime( 2 );           // i = 2
 *        p = prime( 5 );   // prime( i ) = 5
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e = prime( 3 );           // i = 3
 *        p = prime( 7 );   // prime( i ) = 7
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e = prime( 4 );           // i = 4
 *        p = prime( 11);   // prime( i ) = 11
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e=prime( 5 );           // i = 5
 *        p = prime( 13);   // prime( i ) = 13
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e=prime( 6 );           // i = 6
 *        p = prime( 17);   // prime( i ) = 17
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e = prime( 7 );           // i = 7
 *        p = prime( 19);   // prime( i ) = 19
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e=prime( 8 );           // i = 8
 *        p = prime( 23);   // prime( i ) = 23
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e=prime( 9 );           // i = 9
 *        p = prime( 29);   // prime( i ) = 29
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e=prime( 10 );           // i = 10
 *        p = prime(31 );   // prime( i ) = 31
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e=prime( 11 );           // i = 11
 *        p = prime( 37 );   // prime( i ) = 37
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e=prime( 12 );           // i = 12
 *        p = prime( 41 );   // prime( i ) = 41
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e=prime( 13 );           // i = 13
 *        p = prime( 43 );   // prime( i ) = 43
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e=prime( 14 );           // i = 14
 *        p = prime( 47 );   // prime( i ) = 47
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e=prime( 15 );           // i = 15
 *        p = prime( 53 );   // prime( i ) = 53
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e=prime( 16 );           // i = 16
 *        p = prime( 59 );   // prime( i ) = 59
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e=prime( 17 );           // i = 17
 *        p = prime( 61 );   // prime( i ) = 61
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e=prime( 18 );           // i = 18
 *        p = prime( 67 );   // prime( i ) = 67
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e=prime( 19 );           // i = 19
 *        p = prime( 71 );   // prime( i ) = 71
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e=prime( 20 );           // i = 20
 *        p = prime( 73 );   // prime( i ) = 73
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e=prime( 21 );           // i = 21
 *        p = prime( 79 );   // prime( i ) = 79
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e=prime( 22 );           // i = 22
 *        p = prime( 83 );   // prime( i ) = 83
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e=prime( 23 );           // i = 23
 *        p = prime( 89 );   // prime( i ) = 89
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        e=prime( 24 );           // i = 24
 *        p = prime( 97 );   // prime( i ) = 97
 *        n = ( n * p * b.power( e + one ) ) / b.power( e );
 *        p = prime( 2 );   // prime( i ) = 2
 *        m = m * p;
 *        p = prime( 3 );   // prime( i ) = 3
 *        m = m * p;
 *        p = prime( 5 );   // prime( i ) = 5
 *        m = m * p;
 *        p = prime( 7 );   // prime( i ) = 7
 *        m = m * p;
 *        p = prime( 11 );   // prime( i ) = 11
 *        m = m * p;
 *        p = prime( 13);   // prime( i ) = 13
 *        m = m * p;
 *        p = prime( 17);   // prime( i ) = 17
 *        m = m * p;
 *        p = prime( 19);   // prime( i ) = 19
 *        m = m * p;
 *        p = prime( 23);   // prime( i ) = 23
 *        m = m * p;
 *        p = prime( 29);   // prime( i ) = 29
 *        m = m * p;
 *        p = prime( 31);   // prime( i ) = 31
 *        m = m * p;
 *        p = prime( 37);   // prime( i ) = 37
 *        m = m * p;
 *        p = prime( 41);   // prime( i ) = 41
 *        m = m * p;
 *        p = prime( 43);   // prime( i ) = 43
 *        m = m * p;
 *        p = prime( 47);   // prime( i ) = 47
 *        m = m * p;
 *        p = prime( 53) ;  // prime( i ) = 53
 *        m = m * p;
 *        p = prime( 59);   // prime( i ) = 59
 *        m = m * p;
 *        p = prime( 61) ;  // prime( i ) = 61
 *        m = m * p;
 *        p = prime( 67);   // prime( i ) = 67
 *        m = m * p;
 *        p = prime( 71);   // prime( i ) = 71
 *        m = m * p;
 *        p = prime( 73);   // prime( i ) = 73
 *        m = m * p;
 *        p = prime( 79);   // prime( i ) = 79
 *        m = m * p;
 *        p = prime( 83);   // prime( i ) = 83
 *        m = m * p;
 *        p = prime( 89);   // prime( i ) = 89
 *        m = m * p;
 *        p = prime( 97);   // prime( i ) = 97
 *        m = m * p;
 *        //natural b_iter = b.power( natural( iter ) );
 *        //assert(n == b_iter * m);
 *    }
 *
 *    {
 *        natural n(48);
 *        natural one( 1 );
 *        natural two( 2 );
 *        natural e( 10 );          // e_ = 10 
 *        assert(n.power( e ) == n.power( e + one ) / n.power( one ));
 *    }
 */
}
