#include <cassert>
#include <cstdint>
#include <map>
#include <stdexcept>
#include <tuple>

#include <iostream>
/* In this task, you will implement a simple register machine (i.e.
 * a simple model of a computer). The machine has an arbitrary
 * number of integer registers and byte-addressed memory. Registers
 * are indexed from 1 to ‹INT_MAX›. Each instruction takes 2
 * register numbers (indices) and an ‘immediate’ value (an integral
 * constant). Each register can hold a single value of type
 * ‹int32_t› (i.e. the size of the machine word is 4 bytes).  In
 * memory, words are stored LSB-first. The entire memory and all
 * registers start out as 0.
 *
 * The machine has the following instructions (whenever ‹reg_x› is
 * used in the «description», it means the register itself (its
 * value or storage location), not its index; the opposite holds in
 * the column ‹reg_2› which always refers to the register index).
 *
 *  │ opcode │‹reg_2›│ description                                 │
 *  ├────────┼───────┼─────────────────────────────────────────────┤
 *  │ ‹mov›  │  ≥ 1  │ copy a value from ‹reg_2› to ‹reg_1›        │
 *  │        │  = 0  │ set ‹reg_1› to ‹immediate›                  │
 *  │ ‹add›  │  ≥ 1  │ store ‹reg_1 + reg_2› in ‹reg_1›            │
 *  │        │  = 0  │ add ‹immediate› to ‹reg_1›                  │
 *  │ ‹mul›  │  ≥ 1  │ store ‹reg_1 * reg_2› in ‹reg_1›            │
 *  │ ‹jmp›  │  = 0  │ jump to the address stored in ‹reg_1›       │
 *  │        │  ≥ 1  │ jump to ‹reg_1› if ‹reg_2› is nonzero       │
 *  │ ‹load› │  ≥ 1  │ copy value from addr. ‹reg_2› into ‹reg_1›  │
 *  │ ‹stor› │  ≥ 1  │ copy ‹reg_1› to memory address ‹reg_2›      │
 *  │ ‹halt› │  = 0  │ halt the machine with result ‹reg_1›        │
 *  │        │  ≥ 1  │ same, but only if ‹reg_2› is nonzero        │
 *
 * Each instruction is stored in memory as 4 words (addresses
 * increase from left to right). Executing a non-jump instruction
 * increments the program counter by 4 words.
 *
 *  ┌────────┬───────────┬───────┬───────┐
 *  │ opcode │ immediate │ reg_1 │ reg_2 │
 *  └────────┴───────────┴───────┴───────┘
 */

using memory_t = std::map<int32_t, uint8_t>;
using registers_t = std::map<int, int32_t>;

enum class opcode {
    mov = 0,
    add = 1,
    mul = 2,
    jmp = 3,
    load = 4,
    stor = 5,
    hlt = 6
};

class machine {
    memory_t memory;
    registers_t registers;
    int32_t program_counter = 0;
    const int32_t WORD_SIZE = 4;
    const uint8_t FULL_BYTE = 0b11111111;
    
    int32_t immediate() const { return get(program_counter + WORD_SIZE); }
    int32_t reg1_i() const { return get(program_counter + 2 * WORD_SIZE); }
    int32_t reg2_i() const { return get(program_counter + 3 * WORD_SIZE); }
    
    void increment_program_counter() { program_counter += 4 * WORD_SIZE; }

    uint8_t get_1byte(int32_t addr) const;

    void mov();
    void add(); 
    void mul();
    void jmp();
    void load();
    void stor();
    // halt is implemented in run() itself
    bool should_halt(opcode inst);
    void execute_instruction(opcode inst); 


  public:
    /* Read and write memory, one word at a time. */
    int32_t get(int32_t addr) const;
    void set(int32_t addr, int32_t v);

    /* Start executing the program, starting from address zero.
     * Return the value of ‹reg_1› given to the ‹hlt› instruction
     * which halted the computation. */
    int32_t run();
};
