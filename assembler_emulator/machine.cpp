#include "machine.hpp"
/* Implement methods of class ‹machine› here. */

int32_t machine::get(int32_t addr) const {
    int32_t word = 0;
    for (int i = 0; i < WORD_SIZE; i++) {
        word = word | (get_1byte(addr + i) << (8 * i));
    }
    return word;
}

void machine::set(int32_t addr, int32_t v) {
    for (int32_t i = 0; i < WORD_SIZE; i++) {
        memory[addr + i] = (v & (FULL_BYTE << (8 * i))) >> (8 * i);
    }
}

uint8_t machine::get_1byte(int32_t addr) const {
    auto it = memory.find(addr);
    if (it == memory.end())
        return 0;
    return (*it).second;
}

void machine::mov() {
    reg2_i() == 0 ? registers[reg1_i()] = immediate()
                  : registers[reg1_i()] = registers[reg2_i()];
}

void machine::add() {
    reg2_i() == 0
        ? registers[reg1_i()] = registers[reg1_i()] + immediate()
        : registers[reg1_i()] = registers[reg1_i()] + registers[reg2_i()];
}

void machine::mul() {
    if (reg2_i() < 1)
        return;
    registers[reg1_i()] = registers[reg1_i()] * registers[reg2_i()];
}

void machine::jmp() {
    if (reg2_i() == 0 || registers[reg2_i()] != 0) {
        program_counter = registers[reg1_i()];
        program_counter -= 4 * WORD_SIZE;
    }
}

void machine::load() {
    if (reg2_i() < 1)
        return;
    registers[reg1_i()] = get(registers[reg2_i()]);
}

void machine::stor() {
    if (reg2_i() < 1)
        return;
    set(registers[reg2_i()], registers[reg1_i()]);
}

bool machine::should_halt(opcode inst) {
    return inst == opcode::hlt && (reg2_i() == 0 || registers[reg2_i()] != 0);
}

void machine::execute_instruction(opcode inst) {
    switch (inst) {
    case opcode::mov:
        mov();
        break;
    case opcode::add:
        add();
        break;
    case opcode::mul:
        mul();
        break;
    case opcode::jmp:
        jmp();
        break;
    case opcode::load:
        load();
        break;
    case opcode::stor:
        stor();
        break;
    default:
        break;
    }
}

int32_t machine::run() {
    // reset machine
    program_counter = 0;

    auto inst = static_cast<opcode>(get(0));
    execute_instruction(inst);
    while (!should_halt(inst)) {
        increment_program_counter();
        inst = static_cast<opcode>(get(program_counter));
        execute_instruction(inst);
    }
    return registers[reg1_i()];
}

int main() {
    { // BASIC TEST
        machine m;
        m.set(0, static_cast<int32_t>(opcode::mov));
        int32_t a = m.get(0);
        assert(static_cast<opcode>(a) == opcode::mov);

        m.set(4, 10);
        m.set(8, 3);
        m.set(12, 0);
        m.set(16, static_cast<int32_t>(opcode::hlt));
        m.set(24, 3);

        assert(m.run() == 10);
    }

    { // MOV
        machine m;
        m.set(0, static_cast<int32_t>(opcode::mov));
        m.set(4, 420);
        m.set(8, 1);
        m.set(12, 0);

        m.set(16, static_cast<int32_t>(opcode::mov));
        m.set(20, 0);
        m.set(24, 69);
        m.set(28, 1);

        m.set(8 * 4, static_cast<int32_t>(opcode::hlt));
        m.set(10 * 4, 69);
        assert(m.run() == 420);
    }

    { // ADD
        machine m;
        m.set(0 * 4, static_cast<int32_t>(opcode::mov));
        m.set(1 * 4, 420);
        m.set(2 * 4, 1);

        m.set(4 * 4, static_cast<int32_t>(opcode::mov));
        m.set(5 * 4, 10);
        m.set(6 * 4, 69);

        m.set(8 * 4, static_cast<int32_t>(opcode::add)); // 430;
        m.set(10 * 4, 1);
        m.set(11 * 4, 69);

        m.set(12 * 4, static_cast<int32_t>(opcode::add));
        m.set(13 * 4, 1);
        m.set(14 * 4, 1); // 431;

        m.set(16 * 4, static_cast<int32_t>(opcode::hlt));
        m.set(18 * 4, 1);

        assert(m.run() == 431);
    }

    { // MUL
        machine m;
        m.set(0 * 4, static_cast<int32_t>(opcode::mov));
        m.set(1 * 4, 10);
        m.set(2 * 4, 1);

        m.set(4 * 4, static_cast<int32_t>(opcode::mov));
        m.set(5 * 4, 5);
        m.set(6 * 4, 69);

        m.set(8 * 4, static_cast<int32_t>(opcode::mul)); // 50;
        m.set(10 * 4, 1);
        m.set(11 * 4, 69);

        m.set(12 * 4, static_cast<int32_t>(opcode::hlt));
        m.set(14 * 4, 1);

        assert(m.run() == 50);
    }

    { // JMP
        machine m;
        m.set(0 * 4, static_cast<int32_t>(opcode::mov));
        m.set(1 * 4, 12 * 4);
        m.set(2 * 4, 1);

        m.set(4 * 4, static_cast<int32_t>(opcode::jmp));
        m.set(6 * 4, 1);

        m.set(12 * 4, static_cast<int32_t>(opcode::mov));
        m.set(13 * 4, 32 * 4);
        m.set(14 * 4, 2);

        m.set(16 * 4, static_cast<int32_t>(
                          opcode::jmp)); // this jump should be ignored
        m.set(18 * 4, 2);
        m.set(19 * 4, 3000);

        m.set(20 * 4, static_cast<int32_t>(opcode::hlt));
        m.set(22 * 4, 1);

        m.set(32 * 4, static_cast<int32_t>(
                          opcode::mov)); // so that this mov and hlt too
        m.set(33 * 4, 1);
        m.set(34 * 4, 1);

        m.set(36 * 4, static_cast<int32_t>(opcode::hlt));
        m.set(37 * 4, 1);
        assert(m.run() == 12 * 4);
    }

    { // LOAD
        machine m;
        m.set(0, static_cast<int32_t>(opcode::mov));
        m.set(1 * 4, 420 * 4);
        m.set(2 * 4, 1);

        m.set(420 * 4, 999);

        m.set(4 * 4, static_cast<int32_t>(opcode::load));
        m.set(6 * 4, 69);
        m.set(7 * 4, 1);

        m.set(8 * 4, static_cast<int32_t>(opcode::hlt));
        m.set(10 * 4, 69);

        assert(m.run() == 999);
    }

    { // STOR
        machine m;
        m.set(0, static_cast<int32_t>(opcode::mov));
        m.set(1 * 4, 420);
        m.set(2 * 4, 1);

        m.set(4 * 4, static_cast<int32_t>(opcode::stor));
        m.set(6 * 4, 1);
        m.set(7 * 4, 69);

        m.set(8 * 4, static_cast<int32_t>(opcode::load));
        m.set(10 * 4, 5);
        m.set(11 * 4, 69);

        m.set(12 * 4, static_cast<int32_t>(opcode::hlt));
        m.set(14 * 4, 5);

        assert(m.run() == 420);
    }

    {
        machine m;
        m.set(0, 0);  // addr = 0 d = 0
        m.set(4, 32); // addr = 4 d = 32
        m.set(8, 1);  // addr = 8 d = 1
        m.set(12, 0); // addr = 12 d = 0

        m.set(16, 6); // hlt  // addr = 16 d = 6
        m.set(20, 0); //  // addr = 20 d = 0
        m.set(24, 1); // addr = 24 d = 1
        m.set(28, 0); // addr = 28 d = 0
        assert(m.run() == 32);
    }

    {
        // verity test 1
        machine m;
        m.set(0, 0);  // addr = 0 d = 0
        m.set(4, 48); // addr = 4 d = 48
        m.set(8, 1);  // addr = 8 d = 1
        m.set(12, 0); // addr = 12 d = 0
        // mov 48 do reg 1

        m.set(16, 0);  // addr = 16 d = 0
        m.set(20, 12); // addr = 20 d = 12
        m.set(24, 2);  // addr = 24 d = 2
        m.set(28, 0);  // addr = 28 d = 0
        // mov 12 to reg 2

        m.set(32, 0);   // addr = 32 d = 0
        m.set(36, 255); // addr = 36 d = 255
        m.set(40, 4);   // addr = 40 d = 4
        m.set(44, 0);   // addr = 44 d = 0
        // mov 255 to reg 4

        m.set(48, 1);  // addr = 48 d = 1
        m.set(52, -1); // addr = 52 d = -1
        m.set(56, 2);  // addr = 56 d = 2
        m.set(60, 0);  // addr = 60 d = 0
        // add -1 to reg 2

        m.set(64, 1); // addr = 64 d = 1
        m.set(68, 1); // addr = 68 d = 1
        m.set(72, 3); // addr = 72 d = 3
        m.set(76, 0); // addr = 76 d = 0
        // add 1 to reg 3

        m.set(80, 1); // addr = 80 d = 1
        m.set(84, 1); // addr = 84 d = 1
        m.set(88, 4); // addr = 88 d = 4
        m.set(92, 0); // addr = 92 d = 0
        // add 1 to reg 4

        m.set(96, 5);  // addr = 96 d = 5
        m.set(100, 0); // addr = 100 d = 0
        m.set(104, 3); // addr = 104 d = 3
        m.set(108, 4); // addr = 108 d = 4
        // stor content of reg 3 to addres in reg 4

        m.set(112, 3); // addr = 112 d = 3
        m.set(116, 0); // addr = 116 d = 0
        m.set(120, 1); // addr = 120 d = 1
        m.set(124, 2); // addr = 124 d = 2
        // jmp to addres in reg one if reg 2 is nonzero

        m.set(128, 6); // addr = 128 d = 6
        m.set(132, 0); // addr = 132 d = 0
        m.set(136, 4); // addr = 136 d = 4
        m.set(140, 0); // addr = 140 d = 0
        // halt with content of reg 4
        assert(m.run() == 267);
    }

    {
        // verity test 2
        machine m;
        m.set(0, 0);    // addr = 0 d = 0
        m.set(4, 1701); // addr = 4 d = 1701
        m.set(8, 1);    // addr = 8 d = 1
        m.set(12, 0);   // addr = 12 d = 0
        // mov 1701 to 1

        m.set(16, 0);    // addr = 16 d = 0
        m.set(20, 1705); // addr = 20 d = 1705
        m.set(24, 2);    // addr = 24 d = 2
        m.set(28, 0);    // addr = 28 d = 0
        // mov 1705 to 2

        m.set(32, 0);  // addr = 32 d = 0
        m.set(36, 11); // addr = 36 d = 11
        m.set(40, 3);  // addr = 40 d = 3
        m.set(44, 0);  // addr = 44 d = 0
        // mov 11 to 3

        m.set(48, 0);  // addr = 48 d = 0
        m.set(52, 12); // addr = 52 d = 12
        m.set(56, 4);  // addr = 56 d = 4
        m.set(60, 0);  // addr = 60 d = 0
        // mov 12 to 4

        m.set(64, 4); // addr = 64 d = 4
        m.set(68, 0); // addr = 68 d = 0
        m.set(72, 3); // addr = 72 d = 3
        m.set(76, 2); // addr = 76 d = 2
        // load to 3 from addr in 2

        m.set(80, 1);  // addr = 80 d = 1
        m.set(84, -1); // addr = 84 d = -1
        m.set(88, 3);  // addr = 88 d = 3
        m.set(92, 0);  // addr = 92 d = 0
        // add -1 to 3

        m.set(96, 1);  // addr = 96 d = 1
        m.set(100, 1); // addr = 100 d = 1
        m.set(104, 5); // addr = 104 d = 5
        m.set(108, 0); // addr = 108 d = 0
        // add 1 to 5

        m.set(112, 5); // addr = 112 d = 5
        m.set(116, 0); // addr = 116 d = 0
        m.set(120, 3); // addr = 120 d = 3
        m.set(124, 2); // addr = 124 d = 2
        // stor 3 to addr in 2

        m.set(128, 4); // addr = 128 d = 4
        m.set(132, 0); // addr = 132 d = 0
        m.set(136, 4); // addr = 136 d = 4
        m.set(140, 1); // addr = 140 d = 1
        // load to 4 from addr in 1

        m.set(144, 3); // addr = 144 d = 3
        m.set(148, 0); // addr = 148 d = 0
        m.set(152, 4); // addr = 152 d = 4
        m.set(156, 3); // addr = 156 d = 3
        // jmp to addr in 4 if 3 nonzero

        m.set(160, 6); // addr = 160 d = 6
        m.set(164, 0); // addr = 164 d = 0
        m.set(168, 5); // addr = 168 d = 5
        m.set(172, 0); // addr = 172 d = 0

        m.set(1701, 32);
        m.set(1705, 13);
        assert(m.run() == 13);
        assert(m.get(1705) == 0);
    }

    {
        // verity test 3
        machine m;
        m.set(0, 0);   // addr = 0 d = 0
        m.set(4, 73);  // addr = 4 d = 73
        m.set(8, 1);   // addr = 8 d = 1
        m.set(12, 0);  // addr = 12 d = 0
        m.set(16, 0);  // addr = 16 d = 0
        m.set(20, 3);  // addr = 20 d = 3
        m.set(24, 2);  // addr = 24 d = 2
        m.set(28, 0);  // addr = 28 d = 0
        m.set(32, 3);  // addr = 32 d = 3
        m.set(36, 0);  // addr = 36 d = 0
        m.set(40, 1);  // addr = 40 d = 1
        m.set(44, 0);  // addr = 44 d = 0
        m.set(73, 4);  // addr = 73 d = 4
        m.set(77, 0);  // addr = 77 d = 0
        m.set(81, 1);  // addr = 81 d = 1
        m.set(85, 2);  // addr = 85 d = 2
        m.set(89, 6);  // addr = 89 d = 6
        m.set(93, 0);  // addr = 93 d = 0
        m.set(97, 1);  // addr = 97 d = 1
        m.set(101, 0); // addr = 101 d = 0
        assert(m.run() == 73 << 8);
    }
}

