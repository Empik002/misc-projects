const https = require("https");
const express = require("express");
const fs = require('fs');

const app = express();

const options = {
  key: fs.readFileSync(__dirname + "/../certs/key.pem", "utf-8"),
  cert: fs.readFileSync(__dirname + "/../certs/cert.pem", "utf-8"),
  //ciphers: "DEFAULT:!SSLv2:!RC4:!EXPORT:!LOW:!MEDIUM:!SHA1"
};

https.createServer(options, app).listen(3000, () => {
  console.log("Application started and Listening on port 3000");
});

app.use(express.static(__dirname + '/../www')); 
//app.use(express.static(path.join(__dirname, 'www/js/chart'))); 
// save css as static so 
// it will be loaded when sending the html

app.get("/", (_, res) => {
  res.sendFile(__dirname + "/../www/index.html");
});

