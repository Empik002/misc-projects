// this should load all config from a json or a db, for the purposes of the demo it willl be hardcoded in this file

const data_configs = [
    {
        "name": "Eskalačná tabuľka",
        "description": "Pre kontrolu použi select * from idk, pod sibfixom",
        "type": "DB", // this will be calling db
        "db_host": "PSKSIB01",
        "db_user": "SIBDIAG",
        "db_pass": "IDK HOW I WILL DO THIS YET",
        "query": "select * from escl",
        "graph": {
            "type": "bar",
            "axis": {
                // there will be the same stuff bilboard wants for axis
            }
        }
    },
    {
        "name": "Dlhé sessions",
        "description": "Pre kontrolu použi select * from idk, pod sibfixom",
        "type": "DB",
        "db_host": "PSKSIB01",
        "db_user": "SIBDIAG",
        "db_pass": "IDK HOW I WILL DO THIS YET",
        "query": "select dlhýčh sessions",
        "graph": {
            "type": "bar"
        }
    }
];

class DataMgr {
    
    static fetch_data(){
        let graphs = [];

        for (let i = 0; i < data_configs.length; i++){
            if (data_configs[i].type == "DB"){
                graphs.push(this.fetch_db(data_configs[i]));
            }
        }

        return graphs;

    }

    static fetch_db(config){
        // here the query will be handled and assembled into a format
        // bb.generate needs
        let data = {
            "type": config.graph.type,
            "columns": {

            }
        };

        return data;
    }
}
