## Docker Tutorial ##

Learning from this tutorial: _https://www.youtube.com/watch?v=3c-iBn73dDE_

# Docker terminal commands #

Create Image
------------
$ docker build -t name:tag .

-t  specifies the name / name with tags of the image. The dot is the location of the Dockerfile.


Delete image
------------
$ docker rmi < id >


List images
------------
$ docker images

OR:

$ docker image ls


Run images
------------
$ docker run < image_name >
$ sudo docker run --network host -d -p3000:3000 tool 
This command runs the image in "attached" mode (in foreground)
To run in background:

$ docker run -d < image_name > 

-d for detached mode

If you want a specific version (tag) of an image do:

$ docker run image_name:tag

-e flag is Enviromental variapble e.g. -e PATH='bin/'


Stop image
------------
$ docker stop < id >

Id from "docker ps" command


Start image again
------------
$ docker start < id >

Id from "docker ps" command before stopping it (or the -a option)


Show running containers
------------
$ docker ps

This shows only running containers.
To show all:

$ docker ps -a


Name container at startup
-----------
$ docker run --name < name > < image_name >


Show container logs
-----------
$ docker logs < id >


Show conatiner terminal
-----------
$ docker exec -it < id > /bin/bash 

-it stands for interactive terminal, we have to specify the shell
it will create a terminal for the container (it is isolated from the OS)


Docker hub commands
------------
$ docker pull repo_name/image_name 



# Ports #
Application in a container listen to a port of their choosing *BUT* it *is not* an actuall port of the OS / machine. You need to bind the application / container port to the machine port.

To bind the ports do:

$ docker run -p host_port/container_port < image name > 

e.g.

$ docker run -p 6000:6037 testapp


# Docker newtork #
Docker network serves for connecting multiple docker containers. They do not need anything, the whole premise works on docker server relaying the communication by the docker container names.

List networks
------------
$ docker network ls


Create network
-------------
$ docker network create < network_name > 



# Docker File #
It is a blueprint for creating images, you start from an existing image and basically add your stuff to it
It *HAS TO BE NAMED* *Dockerfile*

Example Docker File
-----------

FROM node
ENV PATH='/bin/'
RUN mkdir -p /home/app 
COPY . /home/app 
CMD ["node", "server.js"]

Explanation of the File
-----------
> FROM  - Specifies what image to base this one. 
> ENV   - Set an enviroment variable
> RUN   - Execute any LINUX command (in the container ofc)
> COPY  - Executes on the HOST, copies the files of the app into that image / container
> CMD   - Runs a command (basically run the app iteself). Only one CMD can be present in the app, if there are more only the last one will be executed
