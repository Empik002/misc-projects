## Node.js ##

Learning from this playlist: _https://www.youtube.com/playlist?list=PL4cUxeGkcC9jsz4LDYc6kv3ymONOKxwBU_

Coc.nvim plugins:
:CocInstall coc-json coc-tsserver


# Global Object #
An global object similar to "window" in normal js
Usage: global. smoething

e.g. global.setTimeOut(() => {console.log("a")}, 3000)
__the catual global is not needed__


the variables "\_\_dirname" and "\_\_filename" return the path from where the app was started (without the \ ) 


# Includes #

const name = require('./file.js');
usage: name.function_from_file_js();

*require* autmatically runs the file


# Files #

var fs = require('fs');

fs.writeFile("filename", "what to write", () => {// callback function}); 
// replaces everything; *async*


fs.mkdir(path, (err) => { if (err) {} // callback}); // *async*


# Servers / Clients #

Server
-------
https://stackoverflow.com/questions/31784872/node-js-firefox-error-no-common-encryption-algorithms-error-code-ssl-err

*var http = require('http');*
*const server = http.createServer((req, res) => {} // funkcia runne ked sa niekto pripoji);* (the conts server is not fully needed)

The server function: 

*(req, res) => { console.log(req); }*
// zaloguje request od browsera

*server.listen(port, hostname, () => {// callback pri zacati pocuvania});* // zacne listenovat na requesty od browserov 

je aj HTTPS equivalent, spearatny require('https')
funguje v potdstate rovnako ako http

