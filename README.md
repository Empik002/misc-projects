## Miscellanious projects
# Description 
This repository contains various smaller projects that I have done (and feel
that they are at least a bit worth showcasing). <br> Each folder contains a
separate project with its own README files with build instructions.<br> I
probably wont provide bugfixes or any support on these projects, as these were 
mostly school / weekend projects.

# License
This entire repo is licensed as MIT.
